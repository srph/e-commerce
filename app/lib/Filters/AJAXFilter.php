<?php

class AJAXFilter {

	/**
	 * Protect incoming CSRF attacks from ajax requests
	 *
	 * @return 	void
	 */
	public function filter()
	{
		if(Session::token() != Request::header('x-csrf-token'))
		{
			throw new Illuminate\Session\TokenMismatchException;
		}
	}
}