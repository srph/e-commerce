<?php

interface ItemRepository {

	/**
	 * Create another instance
	 *
	 * @return 	bool
	 */
	public function lode();

	/**
	 * Update the specified instance
	 *
	 * @param 	integer 	$id
	 * @return 	bool
	 */
	public function alter($id);
	
}