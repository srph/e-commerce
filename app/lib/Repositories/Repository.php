<?php

interface Repository {

	/**
	 * Return all results of this instance
	 *
	 * @return 	mixed
	 */
	public function all();

	/**
	 * Find result equal to passed id
	 *
	 * @param 	integer 	$id
	 * @return 	mixed
	 */
	public function find($id);

	/**
	 * Save this instance
	 *
	 * @return 	bool
	 */
	public function save();

	/**
	 * Paginate this instance
	 *
	 * @return 	mixed
	 */
	public function paginate();

	/**
	 * Deletes the specified instance
	 *
	 *
	 * @param 	integer 	$id
	 * @return 	bool
	 */
	public function delete($id);
}