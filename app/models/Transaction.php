<?php

use Carbon\Carbon;

class Transaction extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'transactions';

	/**
	 * Columns fillable by the model
	 *
	 * @var string
	 */
	protected $fillable = array(
		'user_id',
		'item_id',
		'body',
		'created_at',
		'updated_at'
	);

	/**
	 * Validate submitted item
	 *
	 * @return 	mixed
	 */
	public static function validate()
	{
		$rules = array(
			'body'	=>	'required|between:10,1000',
		);

		return Validator::make(Input::all(), $rules);
	}

	/**
	 * ORM with the users table
	 *
	 * @return 	mixed
	 */
	public function user()
	{
		return $this->belongsTo('User');
	}

	/**
	 * ORM with the items table
	 *
	 * @return 	mixed
	 */
	public function item()
	{
		return $this->belongsTo('Item');
	}

	/**
	 * Return a parsed instance of the body
	 *
	 * @return 	string
	 */
	public function parsedBody()
	{
		return Parsedown::instance()->parse($this->body);
	}

	/**
	 * Returns a [Carbon]-ized string of given time
	 *
	 * @return 	string
	 */
	public function timeAgo()
	{
		$date = new Carbon($this->created_at);
		return $date->diffForHumans();
	}

	/**
	 * Update a specified transaction
	 *
	 * @param 	integer 	$id
	 * @return 	bool
	 */
	public static function alter($id)
	{
		$transaction = self::find($id);

		$transaction->body = Input::get('body');
		$transaction->updated_at = new DateTime;

		return ($transaction->saved())
			? true
			: false;
	}

}