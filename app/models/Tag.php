<?php

/**
 * @todo
 */

class Tag extends Eloquent {

	/**
	 * Used to contain an instance of Laravel's
	 * Fluent model.
	 *
	 * @access 	protected
	 * @var 	Tag
	 */
	protected static $instance = null;

	/**
	 * Create an instance of Fluent
	 *
	 *
	 * @param 	string 		$type
	 * @return 	Tag 
	 */
	public static function instance($type)
	{
		$instance = self::$instance;
		if(!is_null($instance)) $instance = null;

		switch($type) {
			case 'tagmap':
				$instance = DB::table('tagmap');
				break;
			case 'tag':
				$instance = DB::table('tags');
				break;
			default:
				return;
		}

		return $instance;
	}

	/**
	 * Fetch requested tag (name or id)
	 *
	 * @param 	int/str 	$request
	 * @return 	Tag
	 */
	public static function fetch($request)
	{
		$tag = self::instance('tag')
			->where('id', '=', $request)
			->orWhere(function($query) use ($request)
			{
				$query->where('name', 'like', '%' . $request .'%');
			})
			->first();

		return $tag;
	}

	/**
	 * Fetch the most latter created tag
	 *
	 * @return 	Tag
	 */
	public static function fetchLast()
	{
		$tag = self::instance('tag')
			->orderBy('id', 'desc')
			->first();

		return $tag;
	}

	/**
	 * Returns the name of the requested tag
	 *
	 * @param 	mixed 	$tag
	 * @return 	mixed
	 */
	public static function name($tag)
	{
		$base = self::base($tag->tag_id);

		return $base->name;
	}

	/**
	 * Create the tag for an item
	 *
	 * @param 	Item 	$item
	 * @param 	Tag		$tag
	 * @return 	void
	 */
	public static function createMap($item, $tag)
	{
		$map = Item::find($item->id)
			->byTag($tag);

		if(count($map) != 0) return;

		// Otherwise, proceed to creating the item tag
		DB::table('tagmap')
			->create(array(
				'item_id'	=>	$item->id,
				'tag_id'	=>	$tag->id
			));
	}

	/**
	 * Create the base tag (tags table)
	 *
	 * @param 	string 	$name
	 * @return 	void
	 */
	public static function createBase($name)
	{
		// Search if tag already exists
		$tag = DB::table('tags')
			->where('name', '=', $name)
			->get();

		// If the tag already exists, simply return
		if(count($tag) !== 0) return;

		// Otherwise, proceed to creating the base tag
		DB::table('tags')
			->create(array('name' => $name));
	}

	/** 
	 * Get the base tag for the requested tag map
	 *
	 * @param 	integer 	$tag
	 * @return 	mixed
	 */
	public static function base($tag)
	{
		return self::instance('tag')
			->where('id', '=', $tag)
			->first();
	}

	/**
	 * Attach tags to items by creating the tag map
	 * (and base tag if it does not exist)
	 *
	 * @param 	Item 	$item
	 * @param 	array 	$data
	 * @return 	void
	 */
	public static function attach($item, $data = array()) {
		foreach($data as $base) {
			if($tag = self::exists($base)) {
				self::createMap($item, $tag);
			} else {
				self::createBase($tag);
				$tag = self::fetchLast();
				self::createMap($item, $tag);
			}
		}
	}

	/**
	 * Checks if a tag exists
	 *
	 * @param 	str/int 	$tag
	 * @return 	boolean
	 */
	public static function exists($tag)
	{
		return (count($return = self::fetchRequest($tag)) > 0)
			? $return
			: false;
	}
}