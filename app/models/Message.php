<?php

use Carbon\Carbon;

class Message extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'messages';

	/**
	 * Columns fillable by the model
	 *
	 * @var string
	 */
	protected $fillable = array(
		'user_id',
		'conversation_id',
		'body',
		'user_hide',
		'recepient_hide',
		'created_at',
		'updated_at'
	);

	/**
	 * Return the user's correspondent with the conversation
	 *
	 * @return 	mixed
	 */
	public function correspondent()
	{
		return ($this->user_id == Auth::user()->id)
			? User::where('id', '=', $this->recepient_id)->first()
			: User::where('id', '=', $this->user_id)->first();
	}

	/**
	 * Returns a [Carbon]-ized string of given time
	 *
	 * @return 	string
	 */
	public function timeAgo()
	{
		$date = new Carbon($this->created_at);
		return $date->diffForHumans();
	}

	/**
	 * ORM with the messages table
	 *
	 * @return 	mixed
	 */
	public function conversation()
	{
		return $this->belongsTo('Conversation');
	}

	/**
	 * ORM with the users table
	 *
	 * @return mixed
	 */
	public function user()
	{
		return $this->belongsTo('User');
	}

}