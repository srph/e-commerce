<?php

use Carbon\Carbon;

class Review extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'reviews';

	/**
	 * Columns fillable by the model
	 *
	 * @var string
	 */
	protected $fillable = array(
		'user_id',
		'profile_id',
		'body',
		'rating',
		'updated_at',
		'created_at'
	);

	/**
	 * Returns a [Carbon]-ized string of given time
	 *
	 * @return 	string
	 */
	public function timeAgo()
	{
		$date = new Carbon($this->created_at);
		return $date->diffForHumans();
	}

	/**
	 * ORM with the users table
	 *
	 * @return 	mixed
	 */
	public function user()
	{
		return $this->belongsTo('User', 'user_id');
	}

	/**
	 * ORM with the profiles table
	 *
	 * @return 	mixed
	 */
	public function profile()
	{
		return $this->belongsTo('Profile', 'profile_id');
	}
}