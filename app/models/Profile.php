<?php

class Profile extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'profiles';

	/**
	 * Columns fillable by the model
	 *
	 * @var string
	 */
	protected $fillable = array(
		'user_id',
		'name',
		'contact_number',
		'location',
		'website',
		'avatar',
		'created_at',
		'updated_at'
	);

	/**
	 * ORM with the users table
	 *
	 * @return 	mixed
	 */
	public function user()
	{
		return $this->belongsTo('User');
	}

	/**
	 * ORM with the profiles table
	 *
	 * @return 	mixed
	 */
	public function reviews()
	{
		return $this->hasMany('Review');
	}

	/**
	 * Select which avatar to display
	 *
	 * @return 	string
	 */
	public function avatar()
	{
		return (is_null($this->avatar))
			? 'http://placehold.it/220x220'
			: url() . '/files/' . $this->avatar;
	}

	/**
	 * Upload the user's avatar
	 *
	 * To simply use:
	 * $this->avatar->upload()
	 *
	 * @return 	boolean
	 */
	public function uploadAvatar($image, $ext)
	{
		// Fetch image information
		$image_path = $image->getRealPath();
		$pixel = Pixel::make($image_path);
		$resize = $pixel->resize(220, 220);

		// Set file upload configurations
		$id = Auth::user()->id;
		$directory = 'files/';
		$file = $directory . $id . $ext;

		// If the avatar was saved
		if($pixel->save($file)) {
			$this->avatar = $file;
			if($this->save()) {
				return true;
			}
		}

		return false;
	}
}