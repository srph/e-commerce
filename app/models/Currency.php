<?php

class Currency extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'currency';

	/**
	 * Columns fillable by the model
	 *
	 * @var string
	 */
	protected $fillable = array(
		'name',
		'symbol'
	);

	/**
	 * ORM with the items table
	 *
	 * @return 	mixed
	 */
	public function items()
	{
		return $this->hasMany('Item');
	}
}