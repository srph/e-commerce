<?php

use Carbon\Carbon;

class Conversation extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'conversations';

	/**
	 * Columns fillable by the model
	 *
	 * @var string
	 */
	protected $fillable = array(
		'user_id',
		'recepient_id',
		'title',
		'user_hide',
		'recepient_hide',
		'user_read',
		'recepient_read',
		'created_at',
		'updated_at'
	);

	/**
	 * Fetch all unread messages
	 *
	 * @return 	mixed
	 */
	public static function getUnread()
	{
		$user = Auth::user();
		$conversations = Conversation::where('user_id', '=', $user->id)
			->where('user_read', '=', false)
			->where('user_hide', '=', false)
			->orWhere(function($query) use ($user)
			{
				$query->where('recepient_id', '=', $user->id)
					->where('recepient_read', '=', false)
					->where('recepient_hide', '=', false);
			})
			->get();
		return $conversations;
	}

	/**
	 * Count unread messages in the inbox
	 *
	 * @return 	integer
	 */
	public static function countUnread()
	{			
		return count(self::getUnread()); 
	}

	/**
	 * Return the user's correspondent with the conversation
	 *
	 * @return 	mixed
	 */
	public function correspondent()
	{
		$id = ($this->user_id == Auth::user()->id)
			? $this->recepient_id
			: $this->user_id;

		return User::where('id', '=', $id)->first();
	}

	/**
	 * Fetch all of the user's conversations
	 *
	 * @return 	mixed
	 */
	public static function fetchAll()
	{
		$user = Auth::user();
		$conversations = Conversation::where('user_id', '=', 1)
			->orWhere(function($query)
			{
				$query->where('recepient_id', '=', 1);
			});

		return $conversations;
	}

	public function lastInteraction()
	{
		$message = $this->messages()->orderBy('id', 'desc')->first();
		$date = new Carbon($message->updated_at);
		return $date->diffForHumans();
	}

	/**
	 * Check if a conversation has already been read
	 *
	 * @return 	bool
	 */
	public function read()
	{
		$user = Auth::user();
		$read = ($this->user_id == $user->id)
			? $this->user_read
			: $this->recepient_read;

		return ($read == true)
			? true
			: false;
	}

	/**
	 * A preview text of the last message of the conversation
	 *
	 * @param 
	 * @return 	string
	 */
	public function preview()
	{
		$message = $this->messages()->orderBy('id', 'desc')->first();
		$body = $message->body;
		$contantenation = (strlen($body) > 50) ? '...' : null;
		return substr($body, 0, 50) . $contantenation;
	}

	/**
	 * ORM with the messages table
	 *
	 * @return 	mixed
	 */
	public function messages()
	{
		return $this->hasMany('Message');
	}

	/**
	 * ORM with the users table
	 *
	 * @return 	mixed
	 */
	public function user()
	{
		return $this->belongsTo('User', 'user_id');
	}

	/**
	 * ORM with the users table (recepient)
	 *
	 * @return 	mixed
	 */
	public function recepient()
	{
		return $this->belongsTo('User', 'recepient_id');
	}

}