<?php

/**
 * @todo Fix item.store and
 * item.update validation
 */

use Carbon\Carbon;

class Item extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'items';

	/**
	 * Columns fillable by the model
	 *
	 * @var string
	 */
	protected $fillable = array(
		'user_id',
		'name',
		'short_description',
		'description',
		'price',
		'currency_id',
		'sold',
		'created_at',
		'updated_at'
	);

	/**
	 * Validate submitted item
	 *
	 * @return 	mixed
	 */
	public static function validate()
	{
		$rules = array(
			'name'			=>	'required|between:10,48',
			'description'	=>	'required|between:10,10000'
		);

		return Validator::make(Input::all(), $rules);
	}

	/**
	 * Checks if a specified item if available
	 *
	 * @return 	string
	 */
	public function availability()
	{
		return ($this->available)
			? "Available"
			: "Sold";
	}

	public function cover()
	{
		$cover = $this->images()->where('cover', '=', 1)->first();
		return $cover;
	}

	/**
	 * Return a parsed instance of the description
	 *
	 * @return 	string
	 */
	public function parsedDescription()
	{
		return Parsedown::instance()->parse($this->description);
	}
	
	/**
	 * Returns a [Carbon]-ized string of given time
	 *
	 * @return 	string
	 */
	public function timeAgo()
	{
		$date = new Carbon($this->created_at);
		return $date->diffForHumans();
	}

	/**
	 * Fetch item tags
	 *
	 * @param 	mixed  	$tag
	 * @return 	mixed
	 */
	public function tags()
	{
		$tags = Tag::instance('tagmap')
			->where('item_id', '=', $this->id)
			->get();

		return $tags;
	}

	/**
	 * Search an item through tags
	 *
	 * @param 	integer / string  	$tag
	 * @return 	mixed
	 */
	public static function byTag($tag)
	{
		// Fetch the requested tag
		$tag = Tag::fetch($tag);

		// Fetch all tag maps that have the same tag
		$maps = Tag::instance('tagmap')
			->where('tag_id', '=', $tag->id)
			->get();

		// Initialize an array to store items that have the same tags
		$items = array();

		// Add each Item object (with the same item id per map) to the array
		foreach($maps as $map) {
			// Item::where
			$items[] = self::where('id', '=', $map->item_id)
				->first();
		}

		// Return the array
		return $items;
	}

	/**
	 *
	 *
	 *
	 * @return 	Response
	 */
	public static function meta($tags)
	{
		if(count($tags) == 0) return;

		$meta = '';
		foreach($tags as $index => $tag) {
			$mark = ($index > 0) ? ',' : '';
			$meta .= $mark . Tag::base($tag->tag_id)->name;
		}

		return $meta;
	}

	/*
	 * --------------------
	 * ORM with other tables
	 * --------------------
	 */

	/**
	 * Relationship with the users table
	 *
	 * @return 	mixed
	 */
	public function user()
	{
		return $this->belongsTo('User');
	}

	/**
	 * Relationship with the transactions table
	 *
	 * @return 	mixed
	 */
	public function transactions()
	{
		return $this->hasMany('Transaction');
	}

	/**
	 * ORM with the currency table
	 *
	 * @return 	mixed
	 */
	public function currency()
	{
		return $this->belongsTo('Currency');
	}

	/**
	 * ORM with the images table
	 *
	 * @return 	mixed
	 */
	public function images()
	{
		return $this->hasMany('Image');
	}
}