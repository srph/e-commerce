<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * Columns fillable by the model
	 *
	 * @var string
	 */
	protected $fillable = array(
		'username',
		'password',
		'email',
		'created_at',
		'updated_at'
	);

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	/**
	 * Checks if user owns the passed variable
	 *
	 * @param 	mixed 	$variable
	 * @return 	bool
	 */
	public static function owns($variable)
	{
		if($variable->user->id == Auth::user()->id) {
			return true;
		}

		return false;
	}

	/*
	 * Validate user creation and account updates
	 *
	 * @param 	string 	$type
	 * @return 	mixed
	 */
	public static function validate($type)
	{
		switch($type) {
			case 'create':
				$rules = array(
					'username'	=>	'required|alpha_dash|between:4,24|unique',
					'password'	=>	'required|alpha_dash|between:6,32',
					'email'		=>	'required|email|between:6,32|unique'
				);
				break;

			case 'password':
				// Custom messages
				$messages = array(
        			'pass_check' => 'Your old password was incorrect',
    			);

    			// A custom password check validator
				Validator::extend('pass_check', function($attribute, $value, $parameters)
   				{   					
					$user = Auth::user();
					$old = Input::get('old_password');
					$password = Hash::check($old, $user->password)
						? Hash::make($old)
						: null;

			        return (Hash::check($old, $user->password))
			        	? true
			        	: false;
		    	});

				$rules = array(
					'old_password'				=>	'required|pass_check',
					'new_password'				=>	'required|between:6,32|alpha_dash|confirmed',
					'new_password_confirmation'	=>	'required|between:6,32|alpha_dash'
				);
				break;

			default:
				return;
		}

		return Validator::make(Input::all(), $rules, $messages);
	}

	/**
	 * ORM with the items table
	 *
	 * @return 	mixed
	 */
	public function items()
	{
		return $this->hasMany('Item');
	}

	/**
	 * ORM with the items table
	 *
	 * @return 	mixed
	 */
	public function reviews()
	{
		return $this->hasMany('Review');
	}

	/**
	 * ORM with the transactions table
	 *
	 * @return 	mixed
	 */
	public function transactions()
	{
		$this->hasMany('Transaction');
	}

	 /**
	 * ORM with the profiles table
	 *
	 * @return 	mixed
	 */
	public function profile()
	{
		return $this->hasOne('Profile');
	}

	/**
	 * ORM with the messages table
	 *
	 * @return 	mixed
	 */
	public function messages()
	{
		return $this->hasMany('Message');
	}

	/**
	 * ORM with the conversations table
	 *
	 * @return 	mixed
	 */
	public function conversations()
	{
		return $this->hasMany('Conversation', 'user_id', 'recepient_id');
	}

	/**
	 * Check if the user is a recepient
	 * 
	 * @param 	integer 	$id
	 * @return 	mixed
	 */
	public static function isRecepientAt($id)
	{
		$conversation = Conversation::find($id);
		return ($conversation->recepient_id == Auth::user()->id)
			? true
			: false;
	}
}