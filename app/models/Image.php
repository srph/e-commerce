<?php

class Image extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var 	string
	 * @access 	protected
	 */
	protected $table = 'images';

	/**
	 * Columns fillable by the model
	 *
	 * @var 	string
	 * @access 	protected
	 */
	protected $fillable = array(
		'item_id',
		'cover',
		'hash',
		'extension',
		'created_at',
		'updated_at'
	);
	
	/**
	 * Upload the image using pixel
	 *
	 * @param 	string 		$file
	 * @param 	integer 	$width
	 * @param 	integer 	$height
	 * @return 	bool
	 */
	public static function upload($image, $hash, $width = 0, $height = 0, $ratio = false, $upsize = true)
	{
		// Resize
		$image_path = $image->getRealPath();
		$pixel = Pixel::make($image_path)->resize($width, $height, $ratio, $upsize);

		// Configurations for file names, paths, extensions
		$ext = '.' . $image->getClientOriginalExtension();
		$directory = 'uploads/';
		$file = $directory . $hash . $ext;

		return ($pixel->save($file)) ? true : false;
	}

	/**
	 * Turns an image into a cover photo (featured photo)
	 *
	 * @return 	boolean
	 */
	public function cover()
	{
		if(!($cover = $this->cover)) {
			$cover = true;
			if($cover->save()) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Fetch the image's filename
	 *
	 * @return 	string
	 */
	public function filename()
	{
		return $this->hash . '.' . $this->extension;
	}

	/**
	 * Return the image's url
	 *
	 * @return 	string
	 */
	public function link()
	{
		return url() . '/uploads/' . $this->filename();
	}

	/**
	 * Hash the file name
	 *
	 * @param 	string 	$image
	 * @return 	string
	 */
	public static function hash($image)
	{
		return Str::random('6', $image);
	}

	/**
	 * Upload multiple images
	 *
	 * @param 	integer 	$id
	 * @param 	arr 		$images
	 * @return 	bool
	 */
	public static function uploadMultiple($images, $id)
	{

		foreach($images as $image) {
			// Create the filename hash and fetch its file extension
			$hash = Str::random('6');
			$extension = $image->getClientOriginalExtension();

			if(self::upload($image, $hash, 562, 562, true)) {
				$image = new Image(array(
					'item_id'		=>	$id,
					'cover'			=>	false,
					'hash'			=>	$hash,
					'extension'		=>	$extension,
					'created_at'	=>	new DateTime,
					'updated_at'	=>	new DateTime
				));

				if($image->save()) {
					continue;
				} else {
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Upload a cover image
	 *
	 * @param 	integer 	$id
	 * @param 	string 		$cover
	 * @return 	bool
	 */
	public static function uploadCover($cover, $id)
	{
		$hash = Str::random('6');
		$extension = $cover->getClientOriginalExtension();
		// Upload and resize it as a cover
		if(self::upload($cover, $hash, 396, 120)) {
			$image = new Image(array(
				'item_id'		=>	$id,
				'cover'			=>	true,
				'hash'			=>	$hash,
				'extension'		=>	$extension,
				'created_at'	=>	new DateTime,
				'updated_at'	=>	new DateTime
			));

			// If image was succesfully saved, return true
			if($image->save()) {
				return true;
			}
		}

		// Otherwise, false.
		return false;
	}

	/**
	 * ORM with the items table
	 *
	 * @return 	mixed
	 */
	public function item()
	{
		return $this->belongsTo('Item');
	}
}