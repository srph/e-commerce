@extends('templates/master')

@section('title')
	- Create a new account!
@stop

@section('style')
	<style>
		.container-small {
			width: 40%;
			margin: 5% auto;
		}
	</style>
@stop

@section('content')
	<div class="container-small">
		@include('templates.alerts')
		<div class="panel panel-default">

			<div class="panel-heading">
				<strong> Create a new account! </strong>
			</div>

			<div class="panel-body">
				{{ Form::open(array('route' => 'user.store', 'role' => 'form')) }}
					{{ Form::token() }}

					<div class="form-group">
						<label> Username </label>
						<div class="input-group">
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-user"></span>
							</span>
							{{ Form::text('username', Input::old('username'), array('class' => 'form-control')) }}
						</div>
					</div>

					@if($errors->has('username'))
						<div class="alert alert-warning">
							{{ $errors->first('username') }}
						</div>
					@endif


					<div class="form-group">
						<label> Email </label>
						<div class="input-group">
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-envelope"></span>
							</span>
							{{ Form::email('email', Input::old('email'), array('class' => 'form-control')) }}
						</div>
						<p class="help-block"><small><span class="glyphicon glyphicon-info-sign"></span></small> Your account will be validated using this email!</p>
					</div>

					@if($errors->has('email'))
						<div class="alert alert-warning">
							{{ $errors->first('email') }}
						</div>
					@endif

					{{-- Password --}}
					<div class="row">

						<div class="col-md-6">
							<div class="form-group">
								<label> Password </label>
								<div class="input-group">
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-lock	"></span>
									</span>
									{{ Form::password('password_confirmation', array('class' => 'form-control')) }}
								</div>
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label> Confirm Password </label>
								{{ Form::password('password', array('class' => 'form-control')) }}
							</div>
						</div>

					</div>

					@if($errors->has('password') || $errors->has('password_confirmation'))
						<div class="alert alert-warning">
							@foreach($errors->get('password') as $message)
								{{ $message }}
							@endforeach

							@foreach($errors->get('password_confirmation') as $message)
								{{ $message}}
							@endforeach
						</div>
					@endif

					<button type="submit" class="btn btn-success btn-block">
						<span class="glyphicon glyphicon-ok"></span>
						 Register
					</button>
				{{ Form::close() }}
			</div>
		</div>
	</div>
@stop