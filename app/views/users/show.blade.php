@extends('templates/master')

@section('title')
	- {{ $user->username }}
@stop

@section('style')
	<style>
		.less-margin {
			margin-top: 1%;
			margin-bottom: 0;
		}

		h2.less-margin {
			margin-top: -3%;
		}

		.green {
			color: #2ecc71;
		}

		.red {
			color: #e74c3c
		}

		.stars {
			margin: 20px 0;
			font-size: 24px;
			color: #2ecc71;
		}
	</style>
@stop

@section('content')

	<div class="row">
		<div class="col-md-3">
			<img class="img-rounded" src="{{ $profile->avatar() }}" width="220" height="220">
			{{-- Profile Name --}}
			@if($profile->name)
				<h3 class="less-margin">
					<strong>
						{{ $profile->name }}
					</strong>
				</h3>
			@endif

			{{-- Username --}}
			<h2 class="less-margin"> <small> <strong> {{ $user->username }} </strong> </small> </h2>

			<hr>

			{{-- User Info  --}}

			@if($profile->location)
				<h5> <span class="glyphicon glyphicon-map-marker"></span> {{ $profile->location }} </h5>
			@endif

			<h5> <span class="glyphicon glyphicon-globe"></span> <a href="#"> {{ $user->email }} </a> </h5>
			
			@if($profile->contact_number)
				<h5> <span class="glyphicon glyphicon-phone-alt"></span> {{ $profile->contact_number }} </h5>
			@endif

			@if($profile->website)
				<h5> <span class="glyphicon glyphicon-link"></span> <a href="#"> {{ $profile->website }} </a> </h5>
			@endif

			@if($profile->created_at)
				<h5> <span class="glyphicon glyphicon-time"></span> Joined on {{ date('F j, Y', strtotime($profile->created_at)) }} </h5>
			@endif

			@if(Auth::check() && $user->id != Auth::user()->id)
				<hr>
				<h5> <span class="glyphicon glyphicon-envelope"></span> <a href="#"> Send message </a> </h5>
			@endif
		</div>

		<div class="col-md-9">
			<ul class="nav nav-tabs">
				<li class="active">
					<a href="#reviews" data-toggle="tab"> User Reviews </a>
				</li>
			</ul>

			<div class="tab-content">
				<div class="tab-pane fade in active" id="reviews">
					<br />
					@if(count($reviews) > 0)
							@foreach($reviews as $review)
							<div class="panel panel-default">
								<div class="panel-body">
									<div class="row">
										<div class="col-md-1">
											<h5 class="text-center">
												<img class="img-circle" src="{{ $review->user->profile->avatar() }}" width="48" height="48" />
											</h5>
										</div>
										<div class="col-md-9">
											<h5> <strong> <a href="{{ URL::route('user.show', $review->user->id) }}">{{ $review->user->username }}</a> </strong> </h5>
											<p> {{ $review->body }} </p>
											@for($i = 1; $i <= 5; $i++)
												<span class="glyphicon glyphicon-star{{ ($i <= $review->rating) ? '' : '-empty'}} @if($review->rating > 2) green @else red @endif"></span>
											@endfor
										</div>

										<div class="col-md-2">
											<h5 class="text-right">
												<span class="glyphicon glyphicon-time"></span>
												<small> {{ $review->timeAgo() }} </small>
											</h5>
										</div>
									</div>
								</div>
							</div>
							@endforeach
						{{ $reviews->links() }}
					@else
						<div class="alert alert-info">
							This user has not received any reviews yet.
						</div>
					@endif

					@if(Auth::guest())
						<div class="alert alert-warning">
							You must be logged in to leave a review to the user's profile
						</div>
					@elseif(Auth::check() && !User::owns($profile))
						<div class="clearfix">
							<div class="pull-right">
								<p>
									<a id="open-review-box" href="#" class="btn btn-success">
										<span class="glyphicon glyphicon-pencil"></span>
										Leave Review
									</a>
								</p>
							</div>
						</div>
						@include('templates.alerts')
						<div id="post-review-box" class="none">
							{{ Form::open(array('route' => 'review.store', 'role' => 'form')) }}
								{{ Form::hidden('ratings', null, array('id' => 'ratings-hidden')) }}
								{{ Form::hidden('profile_id', $profile->id) }}
								<div class="form-group">
									{{ Form::textarea('body', Input::old('body'), array('id' => 'new-review', 'class' => 'form-control animated')) }}
								</div>

								<div class="clearfix">
									<div class="pull-right">

									<p> <div class="stars starrr" data-rating="0"></div> </p>

									<a id="close-review-box" class="btn btn-danger" href="#">
										<span class="glyphicon glyphicon-remove"></span>
										Cancel
									</a>
									{{ Form::submit('Post review!', array('id' => 'review-button', 'class' => 'btn btn-success')) }}
								</div>
							</div>
							{{ Form::close() }}
						</div>
					@endif
				</div>
			</div>
		</div>
	</div>

	<br />
@stop

@section('scripts')
	@if(Auth::check())
		{{ HTML::script('js/starrr.js') }}
		<script>
			$(function() {

				var ratingsField = $('input[name=ratings]');

				$('.starrr').on('starrr:change', function(e, value)
				{
					ratingsField.val(value);
				});

				var reviewBtn = $('#review-button');

				/* Creating reviews */

				reviewBtn.on('click', function(e) {
					e.preventDefault();

					// Send an AJAX
					$.ajax({
						url: "{{ url('review') }}",
						dataType: 'json',
						type: 'POST',
						data: {
							profile_id: $('input[name=profile_id]').val(),
							body: $('textarea[name=body]').val(),
							rating: ratingsField.val()
						},
						beforeSend: function(request) {
							return request.setRequestHeader('X-CSRF-Token', $('meta[name=token]').attr('content'));
							console.log('hey');
						},
						success: function(data) {
							if(data.status)	{
								location.reload();
							}
						}
					});
				});

				/* review styling */

				var reviewBox = $('#post-review-box');
				var newreview = $('#new-review');
				var openreviewBtn = $('#open-review-box');
				var closereviewBtn = $('#close-review-box');

				openreviewBtn.on('click', function(e)
				{
					e.preventDefault();

					reviewBox.slideDown(700);
					openreviewBtn.fadeOut(600);
					closereviewBtn.show();
				});

				closereviewBtn.on('click', function(e)
				{
					e.preventDefault();

					reviewBox.slideUp(600, function()
					{
						openreviewBtn.fadeIn(600);
					});

					closereviewBtn.hide();
				});
			});
		</script>
	@endif
@stop