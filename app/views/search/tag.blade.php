@extends('templates/master')

@section('title')
	- {{ $name }}
@stop

@section('style')
	<style>
		.thumbnail h1,
		.thumbnail h2,
		.thumbnail h3,
		.thumbnail h4,
		.thumbnail h5,
		.thumbnail h6,
		.thumbnail small {
			margin: 0.25em 0.5em 1em 0.5em;
		}

		p.caption {
			font-size: 0.9em;
			margin-left: 0.05em;
			height: 130px;
		}
	</style>
@stop

@section('content')
	{{-- Header --}}
	<h3>
		Results for {{ $name }} <br />
		<small> ({{ count($items) }} RESULTS) </small>
	</h3>
	<hr>

	{{-- Items --}}
	<div class="row">
		@foreach($items as $item)
			<div class="col-md-4">
				<div class="thumbnail">
					<img src="{{ $item->cover()->link() }}" width="396" height="120"/>

					<div class="clearfix">
						<h4 class="pull-right"> <small> {{ $item->currency->symbol }} </small> {{{ $item->price }}} </h4>
						<h4>
							<a href="{{ URL::route('item.show', $item->id) }}"> {{{ $item->name }}} </a>
						</h4>
					</div>

					<p class="caption"> {{ $item->short_description }} </p>
					<br />
					<div class="clearfix">
							<h6 class="pull-right">
								<small> {{ $item->timeAgo() }} </small>
							</h6>
						<h6> <a href="{{ URL::route('user.show', $item->user->id) }}"> {{{ $item->user->username }}} </a> </h6>
					</div>
				</div>
			</div>
		@endforeach
	</div>
@stop