@extends('templates/master')

@section('title')
	- General Account Settings
@stop

@section('content')
	<div class="row">
		<div class="col-md-3">
			<div class="list-group">
				<a href="{{ URL::to('settings/user') }}" class="list-group-item active">
					<span class="glyphicon glyphicon-wrench"></span> Account Settings
				</a>
				<a href="{{ URL::to('settings/profile') }}" class="list-group-item">
					<span class="glyphicon glyphicon-user"></span> Profile Settings
				</a>
			</div>
		</div>

		<div class="col-md-9">
			<div class="panel panel-default">
				<div class="panel-heading">
					<strong> Change Password </strong>
				</div>
				<div class="panel-body">
					@include('templates.alerts')
					@foreach($errors->all() as $message)
						<p>{{ $message }}</p>
					@endforeach
					{{ Form::open(array('action' => 'SettingsController@postUpdatePassword', 'role' => 'form')) }}
						{{ Form::token() }}
						<div class="form-group">
							<label> Current Password </label>
							{{ Form::password('old_password', array('class' => 'form-control')) }}
							<p class="help-block">You are required to enter your current password to change passwords.</p>
						</div>

						<div class="form-group">
							<label> New Password </label>
							{{ Form::password('new_password', array('class' => 'form-control')) }}
							<p class="help-block">Enter a new password consisting of letters and numbers. Passwords are case-sensitive!</p>
						</div>

						<div class="form-group">
							<label> New Password Confirmation </label>
							{{ Form::password('new_password_confirmation', array('class' => 'form-control')) }}
							<p class="help-block">Enter your new password again.</p>
						</div>

						<hr>

						<button type="submit" class="btn btn-success btn-lg">
							<span class="glyphicon glyphicon-ok"></span>
							 Update Password
						</button>
					{{ Form::close()}}
				</div>
			</div>


			<div class="panel panel-default">
				<div class="panel-heading">
					<strong> Delete Account </strong>
				</div>

				<div class="panel-body">
					<p> Once you deactivate your account, there's no turning back. You will lose all of yours items and stats.</p>
					<button type="button" class="btn btn-danger btn-lg">
						<span class="glyphicon glyphicon-remove"></span>
						Deactivate My Account
					</button>
				</div>
			</div>
		</div>
	</div>
@stop