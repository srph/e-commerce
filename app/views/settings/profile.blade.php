@extends('templates/master')

@section('title')
	- Profile Settings
@stop

@section('style')
	{{ HTML::style('css/jquery.fileupload.css') }}
	<style>
		.pull-left {
			margin-right: 0.5em;
		}
	</style>
@stop

@section('content')
	<div class="row">
		<div class="col-md-3">
			<div class="list-group">
				<a href="{{ URL::to('settings/user') }}" class="list-group-item">
					<span class="glyphicon glyphicon-wrench"></span> Account Settings
				</a>
				<a href="{{ URL::to('settings/profile') }}" class="list-group-item active">
					<span class="glyphicon glyphicon-user"></span> Profile Settings
				</a>
			</div>
		</div>

		<div class="col-md-9">
			<div class="panel panel-default">
				<div class="panel-heading">
					<strong> Profile Settings </strong>
				</div>
				<div class="panel-body">
					@include('templates.alerts')
					
					<div class="clearfix">
						<p class="pull-left">
							<input type="image" name="img" src="{{ $profile->avatar() }}" class="img-rounded" width="120" height="120">
							<input id="fileupload" type="file" name="avatar" style="display: none;">
						</p>

						<div id="progress" class="progress progress-striped" style="width: 40%; display: none">
							<div class="progress-bar progress-bar-success">
						</div>
					</div>
					</div>
					<hr>
					{{ Form::open(array('role' => 'form')) }}
						{{ Form::token() }}

						<div class="form-group">
							<label> Name </label>
							{{ Form::text('name', $profile->name, array('class' => 'form-control')) }}
						</div>

						<div class="form-group">
							<label> Location </label>
							{{ Form::text('location', $profile->location, array('class' => 'form-control')) }}
						</div>

						<div class="form-group">
							<label> Contact Number </label>
							{{ Form::text('contact_number', $profile->contact_number, array('class' => 'form-control')) }}
						</div>

						<div class="form-group">
							<label> Website </label>
							{{ Form::text('website', $profile->website, array('class' => 'form-control')) }}
						</div>
						<hr>
						<button type="submit" class="btn btn-success btn-lg">
							<span class="glyphicon glyphicon-ok"></span>
							Submit Changes
						</button>
					{{ Form::close()}}
				</div>
			</div>
		</div>
	</div>
@stop

@section('scripts')
	{{-- jQuery File Upload Scripts --}}
	{{ HTML::script('js/jquery.ui.widget.js') }}
	{{ HTML::script('js/jquery.iframe-transport.js') }}
	{{ HTML::script('js/jquery.fileupload.js') }}
	<script>
		$(function() {
			'use strict';

			var url = "{{ url() }}/ajax/avatar-upload";

			$('input[name=img]').on('click', function() {
				$('input[name=avatar]').click();
			});

			var formdata = false;

			$('input[name=avatar]').on('change', function()
			{
				formdata = new FormData($('input[name=avatar]'));
			});
			


			$('input[name=avatar]').fileupload({
				url: url,
				type: 'POST',
				dataType: 'json',
				data: {
					avatar: $('input[name=avatar]')
				},

				beforeSend: function(request) {
					return request.setRequestHeader('X-CSRF-Token', $('meta[name=token]').attr('content'));
				},

				success: function(data) {
					if(data.status)	{
						//location.reload();
						alert('hey');
						// $('input[name=img]').attr('src')
						// 	.val()
						// 	.replace()
					}
				},

				done: function(e, data) {
					// $.each(data.result.files, function (index, file) {
					// 	$('<p/>').text(file.name).appendTo('#files');
					// });
					//
				},

				progressall: function(e, data) {
					$('#progress').slideDown(500);
					// Compute the total progress
					var progress = parseInt(data.loaded / data.total * 100, 10);

					$('#progress .progress-bar').css(
						'width',
						progress + '%'
					);
				}
			});
		});
	</script>
@stop