@extends('templates/master')

@section('title')
	- Post a new item
@stop

@section('content')
	<div class="panel panel-default">
		<div class="panel-body">
			@include('templates.alerts')
			@if(count($errors->all()) != 0)
				<ul>
				@foreach($errors->all() as $error)
					<li> {{ $error }} </li>
				@endforeach
				</ul>
			@endif
			<h2> Post an Item! </h2>
			<hr>

			{{ Form::open(array('route' => 'item.store', 'role' => 'form', 'files' => true)) }}
				{{ Form::token() }}
				<div class="form-group">
					<label> Item Name </label>
					{{ Form::text('name', Input::old('name'), array('class' => 'form-control')) }}
					<p class="help-block">Post item name less than 50 characters.</p>
				</div>

				<div class="form-group">
					<label> Cover Image </label>
					{{ Form::file('cover') }}
					<p class="help-block">(Allowed filetypes: JPG/JPEG, PNG, GIF; image size: 256 x 256)</p>
				</div>

				<div class="form-group">
					<label> Thumbnails </label>
					<input class="input-file" name="images[]" id="attachments" type="file" multiple>
					<p class="help-block">(Allowed filetypes: JPG/JPEG, PNG, GIF; image size: Minimum: 700x700)</p>
				</div>

				<div class="form-group">
					<label> Short Description </label>
					{{ Form::textarea('short_description', Input::old('short_description'), array('class' => 'form-control', 'rows' => '3')) }}
					<p class="help-block">Make it short and meaningful! (Keep it less than <b> <span id="sd"> 255 </span> </b> characters).</p>
				</div>

				<div class="form-group">
					<label> Item Description </label>
					{{ Form::textarea('description', Input::old('description'), array('class' => 'form-control')) }}
					<p class="help-block">Must not exceed more than 1000 characters.</p>
				</div>

				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<label> Price </label>
							{{ Form::text('price', Input::old('price'), array('class' => 'form-control', 'placeholder' => '1337')) }}
							<p class="help-block">Do not include unnecessary characters.</p>
						</div>

						<div class="col-md-6">
							<label> Currency </label>
							<select name="currency" class="form-control">
								@foreach($currencies as $currency)
									<option value="{{ $currency->id }}"> {{ $currency->name }} ({{ $currency->symbol }}) </option>
								@endforeach
							</select>
						</div>
					</div>
				</div>

				<div class="checkbox">
					<label>
						{{ Form::checkbox('terms_and_conditions', '0', false)}}
						Do you agree to the our Terms and Conditions?
					</label>
				</div>

				<button type="submit" class="btn btn-success">Post Item!</button>
				<a id="cancel" href="#" class="btn btn-danger" data-link="{{ URL::to('/') }}"> Cancel</a>
			{{ Form::close() }}
		</div>
	</div>
@stop

@section('scripts')
	{{ HTML::script('js/bootbox.min.js') }}
	<script>
		$(function() {

			/* Remaining for the short description */
			var desc = $('textarea[name=short_description]');
			desc.keyup(function() {
				var max = 255;
				var textlen = ($(this).val().length);
				var remaining = parseInt(max - textlen, 10);
				$('#sd').text(remaining);
			});


			/* Cancel Item Post */
			cancelBtn = $('a#cancel');

			cancelBtn.on('click', function(e) {
				$this = $(this);
				bootbox.confirm("Would you like to cancel posting this item?", function(result) {
					if(result) {
						window.location.replace($this.data('link'));
					}
				});
			});
		});
	</script>
@stop