@extends('templates.master')

@section('title')
	- {{ $item->name }}
@stop

@section('style')
	<style>
		.panel-footer h1,
		.panel-footer h2,
		.panel-footer h3,
		.panel-footer h4,
		.panel-footer h5,
		.panel-footer h6 {
			margin: 0;
		}

		ul.tagit {
			border: 0;
		}

		ul.tagit li {
			cursor: pointer;
		}

		ul.tagit a {
			color: black;
		}
	</style>
@stop

@section('meta')
	<meta name="description" content="{{ Item::meta($tags) }}">
@stop

@section('content')

	<ol class="breadcrumb">
		<li> <a href="#"> Items </a> </li>
		<li> <a href="#"> Available </a> </li>
		<li class="active"> {{ $item->name }}</a> </li>
	</ol>

	<div class="row">

		{{-- Thumbnails --}}
		<div class="col-md-6">
			<div class="thumbnail">
				{{-- Carousel --}}
				<div id="carousel" class="carousel slide" data-ride="carousel">
					{{-- Indicators --}}
					<ol class="carousel-indicators">
						@foreach($images as $count => $image)
							<li data-target="#carousel" data-slide-to="{{ $count }}"@if($count == 0) class="active"@endif></li>
						@endforeach
					</ol>

					{{-- Slides --}}
					<div class="carousel-inner">
						@foreach($images as $count => $image)
							<div class="item @if($count == 0) active @endif">
								<img src="{{ $image->link() }}" alt="..." width="562" height="562" />
							</div>
						@endforeach
					</div>

					{{-- Controls --}}
					<a class="left carousel-control" href="#carousel" data-slide="prev">
						<span class="glyphicon glyphicon-chevron-left"></span>
					</a>

					<a class="right carousel-control" href="#carousel" data-slide="next">
						<span class="glyphicon glyphicon-chevron-right"></span>
					</a>
				</div>{{-- /.carousel --}}
			</div>
		</div>{{-- /.col-md-6 --}}

		{{-- Item Info --}}
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-body">
					<h4> Item Information </h4>
					<hr>
					<div class="clearfix">
						<h4 class="pull-right text-right">
							<small> {{ $item->currency->symbol }} </small>
							{{ $item->price }} <br />
							<small> {{ strtoupper('PRICE') }} </small>
						</h4>
						<h4>
							{{ $item->name }} <br />
							<small> {{ strtoupper('NAME') }} </small>
						</h4>
					</div>

					<div class="clearfix">
						<h4 class="pull-right text-right">
							Manila <br />							
							<small>{{ strtoupper('LOCATION') }}</small>
						</h4>

						<h4>
							<a href="{{ URL::route('user.show', $item->user->id) }}"> {{ $item->user->username }} </a> <br />
							<small>{{ strtoupper('SELLER') }}</small>
						</h4>
					</div>

					<h4>
						{{ $item->timeAgo() }} <br />
						<small> {{ strtoupper('Date Posted') }} </small>
					</h4>

					<hr>
					<h4> <small> {{ strtoupper('Seller\'s Note') }} </small> </h4>

					<p> {{ $item->description }} </p>

					{{-- Tags --}}
					@if(count($tags) !== 0)
						<hr>
						<h4>
							<small>
								<span class="glyphicon glyphicon-tag"></span>
								 TAGS:
							</small>
						</h4>
						<ul class="tagit">
							@foreach($tags as $tag)
								<a href="{{ URL::to('tag') . '/' . $tag->id }}">
									<code>									
										{{ Tag::name($tag) }}
									</code>
								</a>
							@endforeach
						</ul>
					@endif
				</div>

				<div class="panel-footer">
					<h3 class="text-center">
						<span class="glyphicon glyphicon-envelope"></span>
						Message the Seller
					</h3>
				</div>
			</div>
		</div>

	</div>

	@if(Auth::check())
		<p>
			<a id="open-transaction-box" href="#" class="btn btn-success">
				<span class="glyphicon glyphicon-pencil"></span>
				 Add a transaction!
			</a>
		</p>
	@endif	
	
	<div class="well">

		@if(Auth::check())

			<div id="post-transaction-box" class="none">
				{{ Form::open(array('route' => 'transaction.store', 'role' => 'form')) }}
					<div class="form-group">
						{{ Form::token() }}
						{{ Form::hidden('item_id', $item->id) }}
						{{ Form::textarea('body', Input::old('body'), array('id' => 'new-transaction', 'class' => 'form-control animated')) }}
					</div>

					<div class="clearfix">
						<div class="pull-right">
							<a id="close-transaction-box" class="btn btn-danger" href="#">
								<span class="glyphicon glyphicon-remove"></span>
								Cancel
							</a>
							{{ Form::submit('Add transaction!', array('id' => 'transaction-button', 'class' => 'btn btn-success')) }}
						</div>
					</div>
				{{ Form::close() }}

				<hr>
			</div>
		@else
			<div class="alert alert-info">
				You need to be <a href="{{ URL::to('login') }}" class="alert-link">logged</a> in to make transactions				
			</div>
		@endif

		
			@if(count($transactions) > 0)
				@foreach($transactions as $index => $transaction)
					@if($index >= 1)
						<hr>
					@endif

				<div class="row">

					<div class="col-md-1">
						<h5 class="text-center">
							<img class="img-circle" src="{{ $transaction->user->profile->avatar() }}" width="48" height="48" />
						</h5>
						{{--
						@if(Auth::check())
							@if(User::owns($transaction))
							<a href="#">
								<span class="glyphicon glyphicon-edit"></span>
							</a>

							<a id="transaction-delete" href="#" data-id="{{ $transaction->id }}">
								<span class="glyphicon glyphicon-trash"></span>
							</a>
							@endif
						@endif
						--}}
					</div>

					<div class="col-md-9">
						<h5>
							<strong>
								<a href="{{ URL::route('user.show', $transaction->user->id) }}">
									{{ $transaction->user->username }}
								</a>
							</strong>
						</h5>
						<p> {{ $transaction->body }}  </p>
					</div>

					<div class="col-md-2">
						<h5 class="text-right">
							<span class="glyphicon glyphicon-time"></span>
							<small> {{ $transaction->timeAgo() }} </small>
						</h5>
					</div>

				</div>
				@endforeach
			@else
				<div class="alert alert-info">
					No transactions have been made yet! You may <strong>create</strong> a transaction by simply pressing the button above.
				</div>
			@endif

	</div>

	{{ $transactions->links() }}
@stop

@section('scripts')
	@if(Auth::check())
		{{-- Transactions --}}
		{{ HTML::script('js/bootbox.min.js') }}
		{{-- @include('templates/scripts/editor') --}}
		<script>
			$(function() {

				var transactionBtn = $('#transaction-button');

				/* Creating transactions */

				transactionBtn.on('click', function(e) {
					e.preventDefault();

					// Send an AJAX
					$.ajax({
						url: "{{ url('transaction') }}",
						type: 'POST',
						data: {
							item_id: $('input[name=item_id]').val(),
							body: $('textarea[name=body]').val()
						},
						beforeSend: function(request) {
							return request.setRequestHeader('X-CSRF-Token', $('meta[name=token]').attr('content'));
						},
						success: function(data) {
							if(data.status)	{
								location.reload();
							}
						},
						dataType: 'json'
					});
				});

				/* Deleting transactions */

				var transactionDlt = $('a#transaction-delete');

				transactionDlt.on('click', function(e) {
					e.preventDefault();
					$this = $(this);

					bootbox.confirm("Are you sure to delete this transaction?", function(result)
					{
						if(result) {

							$.ajax({
								url: "{{ url('transaction') }}/" + $this.data('id'),
								type: 'DELETE',
								dataType: 'json',
								success: function(data) {
									if(data.status) {
										location.reload();
									}
								}
							});
						}
					});
				});

				/* Transaction styling */

				var transactionBox = $('#post-transaction-box');
				var newtransaction = $('#new-transaction');
				var opentransactionBtn = $('#open-transaction-box');
				var closetransactionBtn = $('#close-transaction-box');

				opentransactionBtn.on('click', function(e)
				{
					e.preventDefault();

					transactionBox.slideDown(700);
					opentransactionBtn.fadeOut(600);
					closetransactionBtn.show();
				});

				closetransactionBtn.on('click', function(e)
				{
					e.preventDefault();

					transactionBox.slideUp(600, function()
					{
						opentransactionBtn.fadeIn(600);
					});

					closetransactionBtn.hide();
				});
			})();
		</script>
	@endif
@stop