@extends('templates/master')

@section('title')
	- Login
@stop

@section('style')
	<style>
		.container-small {
			width: 40%;
			margin: 5% auto;
		}

		a .or {
			padding-top: 0.6em;
			text-align: center;
			height: 50px;
			width: 50px;
			position: absolute;
			left: 49%;
			background-color: gray;
			border: 3px solid white;
			border-radius: 50%;
			margin: -3% auto auto auto;
		}

		a .or:before {
			content: 'OR';
		}
	</style>
@stop

@section('content')
	<div class="container-small">
		@include('templates.alerts')
		<div class="panel panel-default">

			<div class="panel-heading text-center">
				<strong> User Login </strong>
			</div>

			<div class="panel-body">
				<h5 class="text-center">
					<img class="img-circle" src="{{ asset('img/login-avatar.png') }}" width="98" height="98" />
				</h5>
				{{ Form::open(array('action' => 'AuthenticationController@postLogin', 'role' => 'form')) }}
					{{ Form::token() }}

					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-user"></span>
							</span>
							{{ Form::text('username', Input::old('username'), array('class' => 'form-control', 'placeholder' => 'Username...')) }}
						</div>
					</div>

					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-lock	"></span>
							</span>
						{{ Form::password('password', array('class' => 'form-control', 'placeholder' => '••••••')) }}
						</div>
					</div>
					<button type="submit" class="btn btn-info btn-block">
						 Login
					</button>
				{{ Form::close() }}
			</div>
		</div>

		<a href="{{ URL::route('user.create') }}" class="btn btn-success btn-block btn-lg">
			<div class="or"></div>
			Create an Account
		</a>
	</div>
@stop