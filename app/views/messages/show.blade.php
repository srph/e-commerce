@extends('templates/master')

@section('title')
	- {{ $conversation->correspondent()->username }}
@stop

@section('content')
	<div class="row">

		{{-- Sidebar --}}
		<div class="col-md-3">
			<div class="list-group">
				<a href="{{ URL::route('conversation.index') }}" class="list-group-item">
					<span class="glyphicon glyphicon-envelope"></span> Inbox
					@if(Conversation::countUnread())
						<span class="badge"> {{ Conversation::countUnread() }} </span>
					@endif
				</a>
				<a href="#" class="list-group-item" data-toggle="modal" data-target="#new-conversation">
					<span class="glyphicon glyphicon-pencil"></span> New Conversation
				</a>
			</div>

			<div class="list-group">
				<a href="#" class="list-group-item">
					<span class="glyphicon glyphicon-cog"></span> Settings
				</a>
			</div>
		</div>

		<div class="col-md-9">
			<div class="clearfix">
				<h4> {{ $conversation->correspondent()->username }}
					<div class="pull-right">
						<div class="btn-group">
							<button type="button" class="btn btn-default btn-xs">
								<span class="glyphicon glyphicon-cog"></span>
								 Actions
							</button>
							<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
								<span class="caret"></span>
							    <span class="sr-only">Toggle Dropdown</span>
							</button>
							<ul class="dropdown-menu" role="menu">
								<li>
									<a href="#">
										@if($conversation->read())
											<span class="glyphicon glyphicon-eye-close"></span> Mark as Read
										@else
											<span class="glyphicon glyphicon-eye-open"></span> Mark as Unread
										@endif
									</a>
								</li>
								<li class="divider"></li>
								<li>
									<a href="#">
										<span class="glyphicon glyphicon-minus-sign"></span> Block Conversation
									</a>
								</li>
								<li>
									<a href="#">
										<span class="glyphicon glyphicon-remove"></span> Delete Conversation
									</a>
								</li>
							</ul>
						</div>
					</div>
				</h4>
			</div>
			<hr>
			@foreach($messages as $message)
				<div class="well">
					<div class="row">
						<div class="col-md-1">
							<h5 class="text-center">								
								<img class="img-circle" src="{{ $message->user->profile->avatar() }}" width="48" height="48" />
							</h5>
						</div>

						<div class="col-md-10">
							<h5>
								<strong> {{ $message->user->username }} </strong> <br />
								<small> <span class="glyphicon glyphicon-comment"></span> {{ $message->timeAgo() }} </small>
							</h5>
							{{{ $message->body }}}
						</div>

						<div class="col-md-1">
							<h5 class="text-right">
								<small> <span class="glyphicon glyphicon-chevron-down"></span> </small>								
							</h5>
						</div>
					</div>
				</div>
			@endforeach
			<hr>
			<div class="row">
			{{ Form::open(array('route' => 'message.store')) }}
				{{ Form::token() }}
				{{ Form::hidden('conversation_id', $conversation->id) }}
				<div class="col-md-11">
					{{ Form::textarea('body', Input::old('body'), array('class' => 'form-control', 'rows' => '1')) }}
				</div>

				<div class="col-md-1">
					{{ Form::submit('Send', array('class' => 'btn btn-info')) }}
				</div>
			{{ Form::close() }}
			</div>
		</div>
	</div>
	<br />
@stop

@section('modals')
	@include('templates/modals.new-conversation')
@stop