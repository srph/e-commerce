@extends('templates/master')

@section('title')
	- Messages
@stop

@section('style')
	<style>
		tr.clickable_row:hover {
			cursor: pointer;
		}

		.grey {
			color: gray;
		}

		.grey:hover {
			color: black;
			text-decoration: none;
		}
	</style>
@stop

@section('content')
	<div class="row">

		{{-- Sidebar --}}
		<div class="col-md-3">
			<div class="list-group">
				<a href="#" class="list-group-item active">
					<span class="glyphicon glyphicon-envelope"></span> Inbox
					@if(Conversation::countUnread())
						<span class="badge"> {{ Conversation::countUnread() }} </span>
					@endif
				</a>
				<a href="#" class="list-group-item" data-toggle="modal" data-target="#new-conversation">
					<span class="glyphicon glyphicon-pencil"></span> New Conversation
				</a>
			</div>

			<div class="list-group">
				<a href="#" class="list-group-item">
					<span class="glyphicon glyphicon-cog"></span> Settings
				</a>
			</div>
		</div>

		<div class="col-md-9">
			<div class="panel panel-default">
				<div class="panel-heading">
					<strong>
						Inbox
						@if(!empty($conversations) && Conversation::countUnread())
							<span class="badge"> {{ Conversation::countUnread() }} </span>
						@endif
					</strong>
				</div>
				@if(count($conversations) < 1)
					<div class="panel-body">
						<div class="alert alert-info">
							You have <em>no messages</em>.
							<a href="{{ URL::route('conversation.create') }}" class="alert-link">Send a <em>private</em> message</a> to another user!
						</div>
					</div>
				@else
					<table class="table table-hover">
						<thead>
							<tr>
								<td> <span class="glyphicon glyphicon-user"></span> </td>
								<td> <span class="glyphicon glyphicon-comment"></span> </td>
								<td> <span class="glyphicon glyphicon-calendar"></span> </td>
								<td> </td>
							</tr>
						</thead>
						<tbody>
							@foreach($conversations as $conversation)
								<tr class="clickable_row  @if(!$conversation->read()) active @endif" href="{{ $conversation->id }}">
									<td> {{ $conversation->correspondent()->username }} </td>
									<td> {{ $conversation->preview() }}</td>	
									<td> {{ $conversation->lastInteraction() }} </td>
									<td>
										<a id="conversation-read" href="http://" class="grey" data-id="{{ $conversation->id }}">
											@if($conversation->read())
													<span class="glyphicon glyphicon-eye-close"></span>
											@else
												<span class="glyphicon glyphicon-eye-open"></span>
											@endif
										</a>
										<a href="#" class="grey">
											<span class="glyphicon glyphicon-remove"></span>
										</a>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				@endif
			</div>
		</div>

	</div>
@stop

@section('modals')
	@include('templates/modals.new-conversation')
@stop

@section('scripts')
	<script>
		$(function() {
			$('.clickable_row').on('click', function() {
				console.log("gay");
				$this = $(this);
				window.location.replace("{{ URL::to('conversation') }}/" + $this.attr('href'));
			});

			$('a#conversation-read').on('click', function(e) {
				e.preventDefault();

				$.ajax({
					url: "{{ url('conversation/') }}" + data(id),
					type: 'DELETE',
					dataType: 'json',
					success: function(data) {
						if(data.status) {
							window.location.reload();
						}
					}
				});
			});
		});
	</script>
@stop