<!DOCTYPE html>
<html lang="en">
<head>
	<title> @yield('title') </title>
	{{ HTML::style('css/bootstrap.css') }}
	<style>
		.container {
			margin-top: 1%;
			width: 40%;
		}
	</style>
</head>

<body>
	<div class="container">
		@yield('content')
	</div>

	{{-- Scripts --}}
	{{ HTML::script('js/jquery.js') }}
	{{ HTML::script('js/bootstrap.js') }}
	@yield('scripts')

</body>
</html>