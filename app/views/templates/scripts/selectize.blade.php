{{ HTML::script('js/selectize.js') }}
<script>
	var root = "{{ url() }}";

	$('#searchbox').selectize({
		valueField: 'url',
		labelField: 'name',
		searchField: ['name'],
		maxOptions: 10,
		options: [],
		create: false,
		render: {
			option: function(item, escape) {
				return '<div> <h5>' + escape((item.name === undefined) ? item.username : item.name) + '</h5> </div>';
			}
		},
		optgroups: [
			{
				value: 'item',
				label: 'Items'
			},
			{
				value: 'user',
				label: 'Users'
			}
		],
		optgroupField: 'class',
		optgroupOrder: ['item', 'user'],
		load: function(query, callback) {
			if(!query.length) return callback();
			$.ajax({
				url: root + "/api/search",
				type: 'GET',
				dataType: 'json',
				data: {
					query: $('input[name=query]').val()
				},
				beforeSend: function(request) {
					return request.setRequestHeader('X-CSRF-Token', $('meta[name=token]').attr('content'));
				},
				error: function() {
					callback();
				},
				success: function(result) {
					callback(result.data);
				}
			});
		},
		onChange: function(){
            window.location = this.items[0];
        }
	});
</script>