<!DOCTYPE html>
<html lang="en">
<head>
	<title> {{ Config::get('settings.title') }} @yield('title') </title>
	{{ HTML::style('css/fonts.css')}}
	{{ HTML::style('css/bootstrap.css') }}
	{{ HTML::style('css/stylesheet.css') }}
	{{ HTML::style('css/editor.css') }}
	{{ HTML::style('css/jquery.tagit.css') }}
	{{ HTML::style('css/tagit.ui-zendesk.css') }}
	{{ HTML::style('css/selectize.bootstrap3.css')}}
	@yield('style')
	@yield('meta')
	<meta name="token" content="{{ Session::token() }}">
</head>

<body>
	<div class="container">
		{{-- Navigation --}}
		<nav class="navbar navbar-default navbar-inverse" role="navigation">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>

			<div class="navbar-header">
				<a class="navbar-brand" href="#">E-Commerce</a>
			</div>

			<div class="collapse navbar-collapse" id="navbar">
				<ul class="nav navbar-nav">
					<li> <a href="{{ URL::route('home') }}"> Home </a> </li>
				</ul>


				<ul class="nav navbar-nav navbar-right">

					{{-- Search --}}

					{{ Form::open(array('role' => 'search', 'class' => 'navbar-form navbar-left')) }}
						{{ Form::token() }}
						<div class="form-group" style="width: 240px;">
							<select id="searchbox" name="query" placeholder="Search..." class="form-control"></select>
						</div>
							
					{{ Form::close() }}
					@if(Auth::guest())
						<li> <a href="{{ URL::to('login') }}"> Login </a> </li>
						<li> <a href="{{ URL::route('user.create') }}"> Register </a> </li>
					@else
						<li>
							<a href="{{ URL::route('user.show', Auth::user()->id) }}">
								 Profile
							</a>
						</li>
						<li> <a href="{{ URL::route('conversation.index') }}"> <span class="glyphicon glyphicon-comment"></span> </a> </li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<span class="glyphicon glyphicon-globe"></span></a>
							<ul class="dropdown-menu">
								<li> <a href=""> Kier has made a transaction with one of your items! </a> </li>
								<li class="divider"></li>
								<li> <a> <span class="glyphicon glyphicon-chevron-right"></span> All Notifications </a> </li>
							</ul>
						</li>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<b class="glyphicon glyphicon-cog"></b>
							</a>
							<ul class="dropdown-menu">
								<li>
									<a href="{{ URL::to('settings/user') }}">
										<span class="glyphicon glyphicon-wrench"></span> Account Settings
									</a>
								</li>

								<li>
									<a href="{{ URL::to('settings/profile') }}">
										<span class="glyphicon glyphicon-user"></span> Profile Settings
									</a>
								</li>

								<li>
									<a href="#">
										<span class="glyphicon glyphicon-stats"></span> Dashboard
									</a>
								</li>

								<li class="divider"></li>

								<li>
									<a href="{{ URL::to('logout') }}">
										<span class="glyphicon glyphicon-remove"></span> Logout
									</a>
								</li>

							</ul>
						</li>	
					@endif
				</ul>
			</div>
		</nav>
		{{-- Navigation --}}

		@yield('content')
	</div>

	{{-- Modals --}}
	@yield('modals')

	{{-- Scripts --}}
	{{ HTML::script('js/jquery.js') }}
	{{ HTML::script('js/bootstrap.min.js') }}
	{{ HTML::script('js/tag-it.min.js') }}
	@include('templates/scripts.selectize')
	@yield('scripts')

</body>
</html>