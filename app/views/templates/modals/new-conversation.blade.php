<div class="modal fade" id="new-conversation" tab-index="-1" role="dialog" aria-labelledby="new-conversation-label" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="new-conversation-label"> New Conversation </h4>
			</div>

			<div class="modal-body">
				{{ Form::open(array('conversation.store', 'role' => 'form')) }}
					{{ Form::token() }}

					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon">
								To
      						</span>
      						{{ Form::text('title', null, array('class' => 'form-control')) }}
      					</div>
					</div>

					<div class="form-group">
						{{ Form::textarea('message', null, array('class' => 'form-control')) }}
					</div>
					
					<div class="clearfix">
						<button type="button" class="btn btn-primary pull-right">Send</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>