<?php

class ReviewController extends BaseController {

	/**
	 * Initial configurations for all instance of this controller
	 *
	 * @return 	void
	 */
	public function __construct()
	{
		$this->beforeFilter('ajax', array('only' => array('store', 'update')));
		$this->beforeFilter('auth', array('only' => array('store', 'update')));
	}

	/**
	 * Display a listing of the resource
	 *
	 * @return 	Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Create a new instance of the resource
	 *
	 * @return 	Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store the created resource to the database
	 *
	 * @return 	Response
	 */
	public function store()
	{
		// Validation
		$review = new Review(array(
			'user_id'		=>	Auth::user()->id,
			'profile_id'	=>	Input::get('profile_id'),
			'body'			=>	Input::get('body'),
			'rating'		=>	Input::get('rating'),
			'created_at'	=>	new DateTime,
			'updated_at'	=>	new DateTime
		));

		if($review->save()) {
			Session::flash('success', 'Your review has been posted!');
			return Response::json(array('status' => true));
		}


		Session::flash('error', 'An error has occured');
		return Response::json(array('status' => false));	
	}

	/**
	 * Show the specified resource
	 *
	 * @param 	int 		$id
	 * @return 	Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Edit the specified resource
	 *
	 * @param 	int 		$id
	 * @return 	Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource
	 *
	 * @param 	int 		$id
	 * @return 	Response
	 */
	public function update($id)
	{
		$transaction = Transaction::find($id);

		if(empty($transaction)) {
			App::abort('404', "We are sorry but the requested resource does not exist");
		}

		$transaction = self::find($id);

		$transaction->body = Input::get('body');
		$transaction->updated_at = new DateTime;

		if($transaction->saved()) {
			//
		}
	}

	/**
	 * Delete the specified resource
	 *
	 * @param 	int 		$id
	 * @return 	Response
	 */
	public function destroy($id)
	{
		// Query the database to find exact id
		$transaction = Transaction::find($id);

		if(empty($transaction)) {
			App::abort('404', "We are sorry but the requested resource does not exist");
		}

		// If requested resource has been deleted succesfully
		if(User::owns($transaction)) {
			if($transaction->delete()) {
				Session::flash('success', 'Item was deleted succesfully.');
				return Response::json(array('status' => true));
			}
		}

		Session::flash('error', 'An error has occured while deleting');
		return Response::json(array('status' => false));
	}
}