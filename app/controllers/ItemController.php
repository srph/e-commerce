<?php

/**
 * @todo Validation
 */


class ItemController extends BaseController {

	/**
	 * Initial configurations for all instance of this controller
	 *
	 * @return 	void
	 */
	public function __construct()
	{
		$this->beforeFilter('csrf', array('only' => array('store', 'update')));
		$this->beforeFilter('auth', array('except' => array('show')));
	}

	/**
	 * Display a listing of the resource
	 *
	 * @return 	Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Create a new instance of the resource
	 *
	 * @return 	Response
	 */
	public function create()
	{
		return View::make('items.create')
			->with('currencies', Currency::all());
	}

	/**
	 * Store the created resource to the database
	 *
	 * @return 	Response
	 */
	public function store()
	{
		$validation = Item::validate();

		if($validation->passes()) {

			// Create the item with given input
			$item =  new Item(array(
				'user_id'			=>	Auth::user()->id,
				'name'				=>	Input::get('name'),
				'short_description'	=>	Input::get('short_description'),
				'description'		=>	Input::get('description'),
				'price'				=>	Input::get('price'),
				'currency_id'		=>	Input::get('currency'),
				'sold'				=>	false,
				'created_at'		=>	new DateTime,
				'updated_at'		=>	new DateTime
			));

			// Attach tag for each item
			$tags = Input::get('data');
			Tag::attach($tags);

			// Create and upload item images
			$id = ((int) Item::orderBy('id', 'desc')->first()->id) + 1;
			$cover = Input::file('cover');
			$images = Input::file('images');

			// If upload was succesful
			if(Image::uploadCover($cover, $id) && Image::uploadMultiple($images, $id)) {
				// Save the item
				if($item->save()) {
					$item = Item::orderBy('id', 'desc')->first();

					Session::flash('success', 'Your item has been posted');
					return Redirect::route('item.show', $id);
				}
			}
		}

		Session::flash('error', 'An error has occured!');
		return Redirect::route('item.create')
			->withInput()
			->withErrors($validation);
	}

	/**
	 * Show the specified resource
	 *
	 * @param 	int 		$id
	 * @return 	Response
	 */
	public function show($id)
	{
		$item = Item::find($id);
		
		return View::make('items.show')
			->with('item', $item)
			->with('transactions', $item->transactions()->paginate(10))
			->with('currency', $item->currency)
			->with('images', $item->images()->where('cover', '=', 0)->get())
			->with('tags', $item->tags());
	}

	/**
	 * Edit the specified resource
	 *
	 * @param 	int 		$id
	 * @return 	Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource
	 *
	 * @param 	int 		$id
	 * @return 	Response
	 */
	public function update($id)
	{
		$item = Item::find($id);

		if(empty($item)) {
			App::abort('404', "We are sorry but the requested resource does not exist");
		}

		$validation = Item::validate();

		if($validation->passes()) {
			if(Item::alter()) {
				Session::flash('success', "Your item was updated succesfully");
				return Redirect::route('item.show', $id);
			}
		}
		
		Session::flash('error', "An error has occured");
		return Redirect::route('item.show', $id)
			->withInput()
			->withErrors($validation);
	}

	/**
	 * Delete the specified resource
	 *
	 * @param 	int 		$id
	 * @return 	Response
	 */
	public function destroy($id)
	{
		// Query the database to find exact id
		$item = Item::find($id);

		if(empty($item)) {
			App::abort('404', "We are sorry but the requested resource does not exist");
		}

		// If requested resource has been deleted succesfully
		if(User::owns($item)) {
			if($item->delete()) {
				Session::flash('success', 'Item was deleted succesfully.');
				return Redirect::route('home');
			}
		}

		Session::flash('error', 'An error has occured while deleting');
		return Redirect::route('item.show', $id);
	}
}