<?php

class UserController extends BaseController {
	/**
	 * Display a listing of the resource
	 *
	 * @return 	Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Create a new instance of the resource
	 *
	 * @return 	Response
	 */
	public function create()
	{
		return View::make('users.create');
	}

	/**
	 * Store the created resource to the database
	 *
	 * @return 	Response
	 */
	public function store()
	{
		$validation = User::validate("create");

		if($validation->passes()) {
			$user = new User(array(
				'username'		=>	Input::get('username'),
				'password'		=>	Input::get('password'),
				'email'			=>	Input::get('email'),
				'created_at'	=>	new DateTime,
				'updated_at'	=>	new DateTime,
			));

			$profile = new Profile(array(
				'user_id'		=>	User::orderBy('id', 'desc')->first() + 1,
				'created_at'	=>	new DateTime,
				'updated_at'	=>	new DateTime
			));

			if($user->save() && $profile->save()) {
				Session::flash('success', "Your account has been created");
				return Redirect::route('home');
			}
		}

		Session::flash('error', "An error has occured while processing your registration!");
		return Redirect::route('user.create')
			->withInput()
			->withErrors($validation);
	}

	/**
	 * Show the specified resource
	 *
	 * @param 	int 		$id
	 * @return 	Response
	 */
	public function show($id)
	{
		$user = User::find($id);
		$reviews = Review::where('profile_id', '=', $id)->paginate(10);
		
		return View::make('users.show')
			->with('user', $user)
			->with('profile', $user->profile)
			->with('reviews', $reviews);
	}

	/**
	 * Edit the specified resource
	 *
	 * @param 	int 		$id
	 * @return 	Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource
	 *
	 * @param 	int 		$id
	 * @return 	Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Delete the specified resource
	 *
	 * @param 	int 		$id
	 * @return 	Response
	 */
	public function destroy($id)
	{
		//
	}
}