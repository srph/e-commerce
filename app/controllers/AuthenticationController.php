<?php

class AuthenticationController extends BaseController {

	/**
	 * Initial configurations for all instance of this controller
	 *
	 * @return 	void
	 */
	public function __construct()
	{
		$this->beforeFilter('csrf', array('only' => array('postLogin')));
		$this->beforeFilter('guest', array('only' => array('getLogin')));
	}

	/**
	 * Display the login form
	 *
	 * @return 	Response
	 */
	public function getLogin()
	{
		return View::make('authentication.login');
	}

	/**
	 * Attempt to login the user
	 *
	 * @return 	Response
	 */
	public function postLogin()
	{
		$user = array(
			'username'	=>	Input::get('username'),
			'password'	=>	Input::get('password')
		);

		if(Auth::attempt($user, false)) {
			Session::flash('success', "You have logged in succesfully!");
			return Redirect::intended('/');
		}

		Session::flash('error', "You entered incorrect account credentials!");
		return Redirect::to('login')->withInput();
	}

	/**
	 * Logout the user
	 *
	 * @return 	Response
	 */
	public function getLogout()
	{
		if(Auth::check()) {
			Session::flash('success', "You have been logged out!");
			Auth::logout();
		}

		return Redirect::route('home');
	}
}