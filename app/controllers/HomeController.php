<?php

class HomeController extends BaseController {

	/**
	 * Display the home page along with
	 * the recent list of items being sold
	 *
	 * @return 	Response
	 */
	public function getIndex()
	{
		return View::make('index')
			->with('items', Item::orderBy('created_at', 'desc')->paginate(6));
	}
}