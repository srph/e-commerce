<?php

class MessageController extends BaseController {

	/**
	 * Initial configurations for all instance of this controller
	 *
	 * @return 	void
	 */
	public function __construct()
	{
		$this->beforeFilter('auth', array('only' => array('index', 'create', 'store', 'delete')));
	}

	/**
	 * Display a listing of the resource
	 *
	 * @return 	Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Create a new instance of the resource
	 *
	 * @return 	Response
	 */
	public function create()
	{
		// Compose a message
	}

	/**
	 * Store the created resource to the database
	 *
	 * @return 	Response
	 */
	public function store()
	{
		// Validate
		$user = Auth::user();

		// Fetch & INIT
		$cid = Input::get('conversation_id');
		$conversation = Conversation::find($cid);

		// Update the conversation
		$conversation->updated_at = new DateTime;
		$conversation->recepient_hide = false;
		$conversation->user_hide = false;
		if(User::isRecepientAt($cid)) {
			$conversation->recepient_read = false;
		} else {
			$conversation->user_read = false;
		}


		// Create a new message from the passed input
		$message = new Message(array(
			'user_id'			=>	$user->id,
			'conversation_id'	=>	$cid,
			'body'				=>	Input::get('body'),
			'created_at'		=>	new DateTime,
			'updated_at'		=>	new DateTime,
		));

		if($message->save() && $conversation->save()) {
			Session::flash('success', 'Your message has been sent');
			return Redirect::route('conversation.show', $conversation->id);
		}

		Session::flash('error', 'An error has occured!');
		return Redirect::route('conversation.show', $conversation->id);
	}

	/**
	 * Show the specified resource
	 *
	 * @param 	int 		$id
	 * @return 	Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Edit the specified resource
	 *
	 * @param 	int 		$id
	 * @return 	Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource
	 *
	 * @param 	int 		$id
	 * @return 	Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Delete the specified resource
	 *
	 * @param 	int 		$id
	 * @return 	Response
	 */
	public function destroy($id)
	{
		//
	}
}