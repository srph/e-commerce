<?php

/**
 * Used to handle AJAX requests
 *
 * @todo Transfer all AJAX requests to
 * this controller
 *
 * @todo Finish AJAX Avatar Upload
 */

class AJAXController extends BaseController {

	/**
	 * Apply AJAX CSRF Filter to all controller
	 * functions and instances
	 *
	 * @return 	void
	 */
	public function __construct()
	{
		$this->beforeFilter('ajax', array('only' => 'postAvatarUpload'));
	}

	/**
	 * Upload the user's avatar
	 *
	 * @return 	Response
	 */
	public function postAvatarUpload()
	{
		// $upload_handler = new UploadHandler();
		$user = Auth::user();
		$profile = $user->profile;
		$avatar = Input::file('avatar');
		$ext = $avatar->getClientOriginalExtension();
		$upload = $profile->uploadAvatar($avatar, $ext);

		// If the avatar was succesfully uploaded
		if($upload) {
			$filename = $user->id . '.' . $ext;
			$profile->avatar = $filename;
			if($profile->save()) {
				return Response::json(array('status' => true, 'avatar' => $profile->avatar));
			}
		}

		return Response::json(array('status' => false));
	}
}