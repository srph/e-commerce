<?php

class DashboardController extends BaseController {

	/**
	 *
	 *
	 * @return 	@Response
	 */
	public function getIndex()
	{
		$user = Auth::user();

		return View::make('dashboard.index')
			->with('items', $user->items);
	}
}