<?php

/**
 * @todo Deactivate user accounts
 * Rehash user hash
 * Enable reactivation by sending another email activation
 */

class SettingsController extends BaseController {

	/**
	 * Initial configurations for all instance of this controller
	 *
	 * @return 	void
	 */
	public function __construct()
	{
		$this->beforeFilter('auth', array('only' => array('getUser')));
		$this->beforeFilter('csrf', array('only' => array('postUpdatePassword')));
	}

	/**
	 *
	 *
	 *
	 */
	public function getUser()
	{

		return View::make('settings.user')
			->with('user', Auth::user());
	}

	public function postUpdatePassword()
	{
		// Validate
		$validation = User::validate('password');

		// If the validation passess
		if($validation->passes()) {
			// Update the user's password
			$user = Auth::user();
			$user->password = Hash::make(Input::get('new_password'));
			if($user->save()) {
				Session::flash('success', "Your password has been updated. Please login again!");
				Auth::logout();
				return Redirect::to('settings/user');
			}
		}

		Session::flash('error', 'An error has occured');
		return Redirect::to('settings/user')
			->withErrors($validation);
	}

	/**
	 * Display the form to edit user profile
	 *
	 * @param 	integer 	$id
	 * @return 	Response
	 */
	public function getProfile()
	{
		return View::make('settings.profile')
			->with('profile', Auth::user()->profile);
	}

	/**
	 * Update the user's profile
	 *
	 *
	 * @return 	Response
	 */
	public function postProfile()
	{
		// Validate

		// Update profile
		$profile = Auth::user()->profile;
		$profile->name = e(Input::get('name'));
		$profile->location = e(Input::get('location'));
		$profile->contact_number = e(Input::get('contact_number'));
		$profile->website = e(Input::get('website'));

		// Save
		if($profile->save()) {
			Session::flash('success', 'Your profile has been updated!');
			return Redirect::to('settings/profile');
		}

		Session::flash('error', 'An error has occured while processing your data.');
		return Redirect::to('settings/profile')
			->withErrors();
	}
}