<?php

/**
 * Controls all tag processes
 *
 * @todo Limit through $_GET
 */

class TagController extends BaseController {

	public function getIndex($request)
	{
		// Fetch all tags with the given [[-name-]] or id
		// Fetch all items with the tag(map) of fetched tag
		$items = Item::byTag($request);
		$name = Tag::name(Tag::fetch($request));

		return View::make('search.tag')
			->with('items', $items)
			->with('name', $name);
	}
}