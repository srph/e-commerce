<?php

class ConversationController extends BaseController {

	/**
	 * Initial configurations for all instance of this controller
	 *
	 * @return 	void
	 */
	public function __construct()
	{
		$this->beforeFilter('auth', array('only' => array('index', 'create', 'store', 'delete')));
	}

	/**
	 * Display a listing of the resource
	 *
	 * @return 	Response
	 */
	public function index()
	{
		$conversation = Conversation::fetchAll();

		// Inbox
		return View::make('messages.index')
			->with('conversations', Conversation::fetchAll()->get());
	}

	/**
	 * Create a new instance of the resource
	 *
	 * @return 	Response
	 */
	public function create()
	{
		// Compose a message
	}

	/**
	 * Store the created resource to the database
	 *
	 * @return 	Response
	 */
	public function store()
	{
		// Validate
		$id = Auth::user()->id;
		$recepient = Input::get('recepient');
		$recepient = User::where('username', '=', $recepient)->first();

		$conversation =  new Conversation(array(
			'user_id'			=>	$id,
			'recepient_id'		=>	$recepient->id,
			'created_at'		=>	new DateTime,
			'updated_at'		=>	new DateTime
		));

		$conversation = (Conversation::orderBy('id', 'desc')->first());
		$message = new Message(array(
			'user_id'			=>	$id,
			'conversation_id'	=>	($conversation->id + 1),
			'body'				=>	Input::get('body'),
			'created_at'		=>	new DateTime,
			'updated_at'		=>	new DateTime
		));

		if($conversation->save() && $message->save()) {
			return Response::json(array('status' => true));
		}

		return Response::json(array('status' => false));
	}

	/**
	 * Show the specified resource
	 *
	 * @param 	int 		$id
	 * @return 	Response
	 */
	public function show($id)
	{
		$conversation = Conversation::find($id);
		// Set message to read
		if(User::isRecepientAt($id)) {
			$conversation->recepient_read = true;
		} else {
			$conversation->user_read = true;
		}
		
		if($conversation->save()) {
			return View::make('messages.show')
				->with('conversation', $conversation)
				->with('messages', $conversation->messages()->paginate(10));
		}
	}

	/**
	 * Edit the specified resource
	 *
	 * @param 	int 		$id
	 * @return 	Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource
	 *
	 * @param 	int 		$id
	 * @return 	Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Delete the specified resource
	 *
	 * @param 	int 		$id
	 * @return 	Response
	 */
	public function destroy($id)
	{
		$conversation = Conversation::find($id);
		$user = Auth::user();

		if($user->isRecepientAt($conversation)) {
			$read = $conversation->recepient_read;
			$read = ($read == true) ? false : true;
		} else {
			$read = $conversation->user_read;
			$read = ($read == true) ? false : true;
		}

		if($conversation->save())
			return Response::json(array('status' => true));

		return Response::json(array('status' => false));
	}
}