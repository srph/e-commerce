<?php

/**
 * An API for searching posted items
 * and registered users
 *
 * @todo Search Users
 * via username
 *
 * @todo Search items
 * via hash
 * [/]via item name
 *
 * @todo Escape special HTML characters
 *
 * @todo If a white space preceeded the hash tag
 * Return an error
 *
 * @todo A non-AJAX search
 * Return all possible results
 *
 */

class SearchController extends BaseController {

	/**
	 * Initial configurations for all instance of this controller
	 *
	 * @return 	void
	 */
	public function __construct()
	{
		$this->beforeFilter('ajax', array('only' => array('getIndex')));
	}

	/**
	 * Add key element 'class' to the array
	 *
	 * @param 	array 	$data
	 * @param 	string 	$value
	 * @return 	array
	 */
	public function processClass($data, $value)
	{
		foreach($data as $key => & $item) {
			$item['class'] = $value;
		}

		return $data;
	}

	/**
	 * Add key element 'url' to the array
	 *
	 * @param 	array 	$data
	 * @param 	string 	$value
	 * @return 	array
	 */
	public function processURL($data, $type)
	{
		foreach($data as $key => & $item) {
			$item['url'] = url($type . '/' . $item['id']);
		}

		return $data;
	}

	/**
	 * Add key elements such as 'class' and
	 * 'url' to the classes being invoke by
	 * the AJAX call
	 *
	 * @param 	array 		$data
	 * @param 	string 		$element
	 * @param 	string 		$value
	 * @return 	array
	 */
	public function process(array $data, array $param)
	{
		$figures = array();

		foreach($data as $index => $result) {
			$arg = $param[$index];
			$result = $this->processClass($result, $arg);
			$result = $this->processURL($result, $arg);

			$figures = array_merge($figures, $result);
		}

		return $figures;
	}

	/**
	 * Query the search with the given keywords
	 *
	 * @return 	Response
	 */
	public function getIndex()
	{
		$query = Input::get('query', '');

		// Fetch items
		$items = Item::where('name', 'like', '%' . $query . '%')
			->orderBy('name', 'asc')
			->take(5)
			->get()
			->toArray();

		// Fetch users
		$users = User::where('username', 'like', '%' . $query . '%')
		 	->take(5)
		 	->get()
		 	->toArray();

		// $tags = Item::byTag($query);

		// Add respective class & url for the query
		$data = $this->process(array($items, $users), array('item', 'user'));
		// $items = $this->processClass($items, 'item');
		// $items = $this->processURL($items);
		// $this->processClass($users, 'users');
		// $this->processClass($tags, 'tags');

		// Merge these classes and respond to the call
		// $data = array_merge($items, array($users, $tags));

		return Response::json(array('data' => $data));
	}

	public function processAll($data)
	{
		//
	}
}