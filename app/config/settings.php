<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Default Title
	|--------------------------------------------------------------------------
	|
	| This configuration handles all application title
	|
	| Supported: "file", "cookie", "database", "apc",
	|            "memcached", "redis", "array"
	|
	*/
	'title'				=>	'E-Commerce'

);