<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/**
 * Protect incoming CSRF attacks from ajax requests
 *
 * @return 	void
 */
Route::filter('ajax', 'AJAXFilter');

/**
 * Homepage
 * @link /*
 */
Route::get('/', array(
	'as' => 'home',
	'uses' => 'HomeController@getIndex'
));

/**
 * Item Routes
 * @link item/*
 */
Route::resource('item', 'ItemController');

/**
 * User Routes
 * @link user/*
 */
Route::resource('user', 'UserController');

/**
 * Review Routes
 * @link review/*
 */
Route::resource('review', 'ReviewController');

/**
 * Transaction Messages Routes
 * @link transaction/*
 */
Route::resource('transaction', 'TransactionController');

/**
 * Message & Conversation Routes
 * @link message/
 * @link conversation/*
 */
Route::resource('message', 'MessageController');
Route::resource('conversation', 'ConversationController');

/**
 * Debugging Route
 */
Route::get('debug', (array('as' => 'debug', function()
{
	$tags = Item::byTag('iphone');
	var_dump($tags);
})));

/**
 * Tag Routes
 * @link tag/{tag}
 */
Route::get('tag/{tag}', 'TagController@getIndex');

/**
 * Query Searching Routes
 * @link search/*
 */
Route::get('api/search', 'SearchController@getIndex');

/**
 * AJAX Routes
 * @link ajax/
 */
Route::controller('ajax', 'AJAXController');

/**
 * Account Panel Routes
 * @link settings/
 */
Route::controller('settings', 'SettingsController');

/**
 * Authentication Routes
 * @link /(auth*)
 */
Route::controller('/', 'AuthenticationController');

/**
 * Dashboard Routes
 * @link dashboard/
 */
Route::controller('dashboard', 'DashboardController');