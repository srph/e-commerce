<?php

class TagMapTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('tagmap')->delete();

		$tagmap = array(
			array(
				'id'		=>	1,
				'item_id'	=>	1,
				'tag_id'	=>	1
			),

			array(
				'id'		=>	2,
				'item_id'	=>	1,
				'tag_id'	=>	2
			),

			array(
				'id'		=>	3,
				'item_id'	=>	1,
				'tag_id'	=>	3
			),

			array(
				'id'		=>	4,
				'item_id'	=>	2,
				'tag_id'	=>	1
			),

			array(
				'id'		=>	5,
				'item_id'	=>	2,
				'tag_id'	=>	2
			),
		);

		
		DB::table('tagmap')->insert($tagmap);
	}
}