<?php

class TransactionsTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('transactions')->delete();

		$comments = array(
			array(
				'id'			=>	1,
				'user_id'		=>	1,
				'item_id'		=>	1,
				'body'			=>	'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
				'created_at'	=>	new DateTime,
				'updated_at'	=>	new DateTime
			),

			array(
				'id'			=>	2,
				'user_id'		=>	1,
				'item_id'		=>	1,
				'body'			=>	'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
				'created_at'	=>	new DateTime,
				'updated_at'	=>	new DateTime
			)
		);

		DB::table('transactions')->insert($comments);
	}
}