<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('UsersTableSeeder');
		$this->command->info('The [users] table has been seeded succesfully!');
		$this->call('ItemsTableSeeder');
		$this->command->info('The [items] table has been seeded succesfully!');
		$this->call('TransactionsTableSeeder');
		$this->command->info('The [transactions] table has been seeded succesfully!');
		$this->call('CurrencyTableSeeder');
		$this->command->info('The [currency] table has been seeded succesfully!');
		$this->call('ProfilesTableSeeder');
		$this->command->info('The [profiles] table has been seeded succesfully!');
		$this->call('ImagesTableSeeder');
		$this->command->info('The [images] table has been seeded succesfully!');
		$this->call('ConversationsTableSeeder');
		$this->command->info('The [conversations] table has been seeded succesfully!');
		$this->call('MessagesTableSeeder');
		$this->command->info('The [messages] table has been seeded succesfully!');
		$this->call('ReviewsTableSeeder');
		$this->command->info('The [review] table has been seeded succesfully!');
		$this->call('TagMapTableSeeder');
		$this->command->info('The [tagmap] table has been seeded succesfully!');
		$this->call('TagsTableSeeder');
		$this->command->info('The [tags] table has been seeded succesfully!');
	}

}