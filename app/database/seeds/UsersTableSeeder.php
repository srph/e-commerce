<?php

class UsersTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('users')->delete();

		$users = array(
			array(
				'id'			=>	1,
				'username'		=>	'seraphipod',
				'password'		=>	Hash::make('1234'),
				'email'			=>	'seraphipod@gmail.com',
				'created_at'	=>	new DateTime,
				'updated_at'	=>	new DateTime
			),

			array(
				'id'			=>	2,
				'username'		=>	'xoxo',
				'password'		=>	Hash::make('1234'),
				'email'			=>	'xoxo@gmail.com',
				'created_at'	=>	new DateTime,
				'updated_at'	=>	new DateTime
			)
		);

		DB::table('users')->insert($users);
	}
}