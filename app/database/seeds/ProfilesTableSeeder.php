<?php

class ProfilesTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('profiles')->delete();

		$profiles = array(
			array(
				'id'				=>	1,
				'user_id'			=>	1,
				'name'				=>	'Epsilon Gamma',
				'contact_number'	=>	'8711452',
				'location'			=>	'Philippines',
				'website'			=>	'http://kier.com',
				'avatar'			=>	'1.png',
				'created_at'		=>	new DateTime,
				'updated_at'		=>	new DateTime
			),

			array(
				'id'				=>	2,
				'user_id'			=>	2,
				'name'				=>	'Ramon Bautista',
				'contact_number'	=>	'8711452',
				'location'			=>	'Philippines',
				'website'			=>	'http://kier.com',
				'avatar'			=>	'1.png',
				'created_at'		=>	new DateTime,
				'updated_at'		=>	new DateTime
			),
		);

		
		DB::table('profiles')->insert($profiles);
	}
}