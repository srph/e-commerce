<?php

class ItemsTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('items')->delete();

		$items = array(
			array(
				'id'				=>	1,
				'user_id'			=>	1,
				'name'				=>	'Juke Box',
				'short_description'	=>	'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
				'description'		=>	'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',
				'price'				=>	'32000',
				'sold'				=>	false,
				'currency_id'		=>	1,
				'created_at'		=>	new DateTime,
				'updated_at'		=>	new DateTime
			),

			array(
				'id'				=>	2,
				'user_id'			=>	2,
				'name'				=>	'Juke Box',
				'short_description'	=>	'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
				'description'		=>	'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',
				'price'				=>	'32000',
				'currency_id'		=>	2,
				'sold'				=>	false,
				'created_at'		=>	new DateTime,
				'updated_at'		=>	new DateTime
			),

			array(
				'id'				=>	3,
				'user_id'			=>	1,
				'name'				=>	'Hipster Glasses',
				'short_description'	=>	'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
				'description'		=>	'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',
				'price'				=>	'32000',
				'sold'				=>	false,
				'currency_id'		=>	1,
				'created_at'		=>	new DateTime,
				'updated_at'		=>	new DateTime
			)
		);

		DB::table('items')->insert($items);
	}
}