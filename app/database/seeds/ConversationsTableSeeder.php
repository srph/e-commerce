<?php

class ConversationsTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('conversations')->delete();

		$conversations = array(
			array(
				'id'			=>	1,
				'user_id'		=>	1,
				'recepient_id'	=>	2,
				'created_at'	=>	new DateTime,
				'updated_at'	=>	new DateTime
			),

			array(
				'id'			=>	2,
				'user_id'		=>	2,
				'recepient_id'	=>	1,
				'created_at'	=>	new DateTime,
				'updated_at'	=>	new DateTime
			),
		);

		
		DB::table('conversations')->insert($conversations);
	}
}