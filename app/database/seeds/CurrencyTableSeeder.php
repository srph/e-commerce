<?php

class CurrencyTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('currency')->delete();

		$currency = array(
			array(
				'id'		=>	1,
				'name'		=>	'U.S. Dollar',
				'symbol'	=>	'&#36;',
			),

			array(
				'id'		=>	2,
				'name'		=>	'Philippine Peso',
				'symbol'	=>	'PHP'
			),
		);

		
		DB::table('currency')->insert($currency);
	}
}