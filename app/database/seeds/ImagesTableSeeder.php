<?php

class ImagesTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('images')->delete();

		$images = array(
			array(
				'id'			=>	1,
				'item_id'		=>	1,
				'cover'			=>	1,
				'hash'			=>	'4iH4Jw',
				'extension'		=>	'gif',
				'created_at'	=>	new DateTime,
				'updated_at'	=>	new DateTime
			),

			array(
				'id'			=>	2,
				'item_id'		=>	3,
				'cover'			=>	1,
				'hash'			=>	'p4h2Cj',
				'extension'		=>	'gif',
				'created_at'	=>	new DateTime,
				'updated_at'	=>	new DateTime
			),

			array(
				'id'			=>	3,
				'item_id'		=>	2,
				'cover'			=>	1,
				'hash'			=>	'p4h2Cj',
				'extension'		=>	'gif',
				'created_at'	=>	new DateTime,
				'updated_at'	=>	new DateTime
			),

			array(
				'id'			=>	4,
				'item_id'		=>	1,
				'cover'			=>	0,
				'hash'			=>	'19qYW5',
				'extension'		=>	'gif',
				'created_at'	=>	new DateTime,
				'updated_at'	=>	new DateTime
			),

			array(
				'id'			=>	5,
				'item_id'		=>	1,
				'cover'			=>	0,
				'hash'			=>	'C1T7hi',
				'extension'		=>	'gif',
				'created_at'	=>	new DateTime,
				'updated_at'	=>	new DateTime
			),

			array(
				'id'			=>	6,
				'item_id'		=>	2,
				'cover'			=>	0,
				'hash'			=>	'cEuX6a',
				'extension'		=>	'gif',
				'created_at'	=>	new DateTime,
				'updated_at'	=>	new DateTime
			),

			array(
				'id'			=>	7,
				'item_id'		=>	2,
				'cover'			=>	0,
				'hash'			=>	'DSQb3r',
				'extension'		=>	'gif',
				'created_at'	=>	new DateTime,
				'updated_at'	=>	new DateTime
			),

			array(
				'id'			=>	8,
				'item_id'		=>	3,
				'cover'			=>	0,
				'hash'			=>	'f3spSj',
				'extension'		=>	'gif',
				'created_at'	=>	new DateTime,
				'updated_at'	=>	new DateTime
			),

			array(
				'id'			=>	9,
				'item_id'		=>	3,
				'cover'			=>	0,
				'hash'			=>	'f3spSj',
				'extension'		=>	'gif',
				'created_at'	=>	new DateTime,
				'updated_at'	=>	new DateTime
			),
		);

		
		DB::table('images')->insert($images);
	}
}