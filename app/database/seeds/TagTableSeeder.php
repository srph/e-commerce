<?php

class TagsTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('tags')->delete();

		$tags = array(
			array(
				'id'		=>	1,
				'name'		=>	'iphone'
			),

			array(
				'id'		=>	2,
				'name'		=>	'android'
			),

			array(
				'id'		=>	3,
				'name'		=>	'rigs'
			),

			array(
				'id'		=>	4,
				'name'		=>	'engineer'
			),
		);

		
		DB::table('tags')->insert($tags);
	}
}