<?php

class MessagesTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('messages')->delete();

		$messages = array(
			array(
				'id'				=>	1,
				'user_id'			=>	1,
				'conversation_id'	=>	1,
				'body'				=>	'Hello World!',
				'created_at'		=>	new DateTime,
				'updated_at'		=>	new DateTime
			),

			array(
				'id'				=>	2,
				'user_id'			=>	2,
				'conversation_id'	=>	1,
				'body'				=>	'This is a message',
				'created_at'		=>	new DateTime,
				'updated_at'		=>	new DateTime
			),

		array(
				'id'				=>	3,
				'user_id'			=>	2,
				'conversation_id'	=>	2,
				'body'				=>	'Hello World!',
				'created_at'		=>	new DateTime,
				'updated_at'		=>	new DateTime
			),

			array(
				'id'				=>	4,
				'user_id'			=>	1,
				'conversation_id'	=>	2,
				'body'				=>	'This is a message',
				'created_at'		=>	new DateTime,
				'updated_at'		=>	new DateTime
			),
		);

		
		DB::table('messages')->insert($messages);
	}
}