<?php

use Illuminate\Database\Migrations\Migration;

class CreateConversationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('conversations', function($table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('recepient_id');
			$table->boolean('user_hide')->default(false);
			$table->boolean('recepient_hide')->default(false);
			$table->boolean('user_read')->default(false);
			$table->boolean('recepient_read')->default(false);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('conversations');
	}

}