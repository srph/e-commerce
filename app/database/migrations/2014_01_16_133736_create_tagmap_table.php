<?php

use Illuminate\Database\Migrations\Migration;

class CreateTagmapTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tagmap', function($table)
		{
			$table->increments('id');
			$table->integer('item_id');
			$table->integer('tag_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tagmap');
	}

}