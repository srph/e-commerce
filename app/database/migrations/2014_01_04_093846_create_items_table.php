<?php

use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('items', function($table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->string('name');
			$table->string('short_description');
			$table->text('description');
			$table->string('price');
			$table->integer('currency_id');
			$table->boolean('sold')->default(false);
			$table->string('thumbnail')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('items');
	}

}